//
//  FirstViewController.m
//  YouPick
//
//  Created by Admin on 23/07/14.
//  Copyright (c) 2014 Syscraft. All rights reserved.
//

#import "FirstViewController.h"
#import "LoginViewController.h"
#import "HomeViewController.h"
#import "ExploreViewController.h"
#import "CameraViewController.h"
#import "ProfileViewController.h"
#import "RewardsViewController.h"
@interface FirstViewController ()

@end

@implementation FirstViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)loginAction:(id)sender{
    LoginViewController *login=[[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    [self presentViewController:login animated:YES completion:nil];
}
-(IBAction)fbLoginAction:(id)sender{
    NSArray *permissions = [[NSArray alloc] initWithObjects:
                            @"user_likes",
                            @"user_birthday",
                            @"email",
                            @"publish_stream",
                            @"publish_actions",
                            @"user_photos",
                            @"friends_photos",
                            @"read_stream",
                            @"user_about_me",
                            @"user_activities",
                            @"read_friendlists",
                            @"user_friends",
                            @"offline_access",
                            nil];
    session=[[FBSession alloc] initWithAppID:@"810645035633294" permissions:permissions urlSchemeSuffix:nil tokenCacheStrategy:nil];
    NSHTTPCookieStorage *storage=[NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (cookie in [storage cookies]) {
        [storage deleteCookie:cookie];
    }
    [[FBSession activeSession] handleDidBecomeActive];
    [[FBSession activeSession] closeAndClearTokenInformation];
    [FBSession setActiveSession:session];
    
    FBSessionLoginBehavior behaviour=FBSessionLoginBehaviorForcingWebView;
    [session openWithBehavior:behaviour completionHandler:^(FBSession *session1,FBSessionState status,NSError *error){
        if (error) {
            
        }else{
            [FBRequestConnection startForMeWithCompletionHandler:^(FBRequestConnection *connection,id result,NSError *error){
                if (!error) {
                    NSLog(@"hhhh %@",result);
                    
                    UITabBarController *tab=[[UITabBarController alloc] init];
                    
                    HomeViewController *home=[[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
                    ExploreViewController *explore=[[ExploreViewController alloc] initWithNibName:@"ExploreViewController" bundle:nil];
                    CameraViewController *cam=[[CameraViewController alloc] initWithNibName:@"CameraViewController" bundle:nil];
                    ProfileViewController *profile=[[ProfileViewController alloc] initWithNibName:@"ProfileViewController" bundle:nil];
                    RewardsViewController *rewards=[[RewardsViewController alloc] initWithNibName:@"RewardsViewController" bundle:nil];
                    tab.viewControllers=[NSArray arrayWithObjects:home,explore,cam,profile,rewards, nil];
                    [tab.tabBar setBackgroundColor:[UIColor clearColor]];
                    UITabBar *tabBar = tab.tabBar;
                    UITabBarItem *item0 = [tabBar.items objectAtIndex:0];
                    UITabBarItem *item1 = [tabBar.items objectAtIndex:1];
                    UITabBarItem *item2 = [tabBar.items objectAtIndex:2];
                    UITabBarItem *item3 = [tabBar.items objectAtIndex:3];
                    UITabBarItem *item4 = [tabBar.items objectAtIndex:4];
                    
                    
                    [item0 setImage:[[UIImage imageNamed:@"home.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
                    [item0 setSelectedImage:[[UIImage imageNamed:@"home_h.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
                    [item0 setImageInsets: UIEdgeInsetsMake(6, 0, -6, 0)];
                    [item0 setTitlePositionAdjustment: UIOffsetZero];
                    
                    [item1 setImage:[[UIImage imageNamed:@"explore.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
                    [item1 setSelectedImage:[[UIImage imageNamed:@"explore_h.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
                    [item1 setImageInsets: UIEdgeInsetsMake(6, 0, -6, 0)];
                    [item1 setTitlePositionAdjustment: UIOffsetZero];
                    
                    [item2 setImage:[[UIImage imageNamed:@"cam.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
                    [item2 setSelectedImage:[[UIImage imageNamed:@"cam_h.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
                    [item2 setImageInsets: UIEdgeInsetsMake(6, 0, -6, 0)];
                    [item2 setTitlePositionAdjustment: UIOffsetZero];
                    
                    [item3 setImage:[[UIImage imageNamed:@"profile.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
                    [item3 setSelectedImage:[[UIImage imageNamed:@"profile_h.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
                    [item3 setImageInsets: UIEdgeInsetsMake(6, 0, -6, 0)];
                    [item3 setTitlePositionAdjustment: UIOffsetZero];
                    
                    [item4 setImage:[[UIImage imageNamed:@"rewards.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
                    [item4 setSelectedImage:[[UIImage imageNamed:@"rewards_h.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
                    [item4 setImageInsets: UIEdgeInsetsMake(6, 0, -6, 0)];
                    [item4 setTitlePositionAdjustment: UIOffsetZero];
                    [[UITabBar appearance] setBackgroundImage:[UIImage imageNamed:@""]];
                    [[UITabBar appearance] setTintColor:[UIColor clearColor]];
                    [self presentViewController:tab animated:NO completion:nil];

                    
                    
                }
            }];
        }
    }];
}

-(IBAction)forgotPassAction:(id)sender{
    
}

@end
