//
//  FirstViewController.h
//  YouPick
//
//  Created by Admin on 23/07/14.
//  Copyright (c) 2014 Syscraft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
@interface FirstViewController : UIViewController{
    FBSession *session;
    NSHTTPCookie *cookie;
}
-(IBAction)loginAction:(id)sender;
-(IBAction)fbLoginAction:(id)sender;
-(IBAction)forgotPassAction:(id)sender;
@end
