//
//  ProfileViewController.h
//  YouPick
//
//  Created by Admin on 23/07/14.
//  Copyright (c) 2014 Syscraft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate>
{
    UICollectionViewFlowLayout *flowLayout;
    IBOutlet UIImageView *profileImg;
    IBOutlet UICollectionView *colViewObj;
    IBOutlet UIButton *likebtn;
    IBOutlet UIButton *dislikeBtn;
    IBOutlet UIButton *followerBtn;
    IBOutlet UILabel *likeLbl;
    IBOutlet UILabel *dislikeLbl;
    IBOutlet UILabel *followerLbl;

}
@end
