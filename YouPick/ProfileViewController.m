//
//  ProfileViewController.m
//  YouPick
//
//  Created by Admin on 23/07/14.
//  Copyright (c) 2014 Syscraft. All rights reserved.
//

#import "ProfileViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "IGPhotoCell.h"
@interface ProfileViewController ()

@end

@implementation ProfileViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if ([UIScreen mainScreen].bounds.size.height==568) {
        likebtn.frame=CGRectMake(-2, 200, 110, 50);
    }
    [colViewObj registerNib:[UINib nibWithNibName:@"IGPhotoCell" bundle:nil] forCellWithReuseIdentifier:@"cellID"];

    profileImg.layer.cornerRadius = profileImg.frame.size.height /2;
    profileImg.layer.masksToBounds = YES;
    profileImg.layer.borderWidth = 0;

    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - UICollectionViewDatasource

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 10;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellId=@"cellID";
    IGPhotoCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:cellId forIndexPath:indexPath];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        //NSData *data1=[NSData dataWithContentsOfURL:[NSURL URLWithString:[[[[self.data objectAtIndex:indexPath.row] objectForKey:@"images"] objectForKey:@"thumbnail"] objectForKey:@"url"]]];
        dispatch_async(dispatch_get_main_queue(), ^{
            cell.img.image=[UIImage imageNamed:@"sampleImg.png"];
            //cell.img.image=[UIImage imageWithData:data1];
        });
    });
    return cell;
    
}

@end
