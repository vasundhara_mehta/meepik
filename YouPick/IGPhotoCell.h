//
//  IGPhotoCell.h
//  InstagramClient
//
//  Created by Admin on 21/07/14.
//  Copyright (c) 2014 Crino. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IGPhotoCell : UICollectionViewCell
@property(strong,nonatomic)IBOutlet UIImageView *img;
@end
